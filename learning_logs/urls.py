"""Defines URLs specific to the learning logs app"""

from django.urls import path

from . import views

app_name = 'learning_logs'
urlpatterns = [
    # Home Page
    path('', views.index, name="index"),
    # Topics Page
    path('topics/', views.topics, name="topics"),
    # Individual Topic Page
    path('topics/<int:topic_id>/', views.topic, name="topic"),
    # New Topic Page
    path('new_topic/', views.new_topic, name="new_topic"),
    # New Entry Page
    path('topics/<int:topic_id>/new_entry/', views.new_entry, name="new_entry")
]
